FROM nanjiang/common-ubuntu
LABEL maintainer "Nanjiang Shu (nanjiang.shu@nbis.se)"
LABEL version "1.0"

#================================
# Install basics
#===============================
RUN apt-get  update -y
RUN apt-get install -y apt-utils  \
                       curl wget vim tree bc git \
                       python python-dev python-pip \
                       build-essential  \
                       libxml2-dev libxslt1-dev libsqlite3-dev zlib1g-dev   \
                       r-base \
                       cmake  \
                       qt4-qmake  \
                       locales-all \
                       emboss-lib   \
                       emboss

RUN apt-get install -y ncbi-blast+ blast2

#================================
#  Add subcons source code
#===============================
WORKDIR /app
# add the source code to WORKDIR /app
ADD . ./subcons 


RUN mkdir -p /app/download /data/  /scratch/ /static
#================================
# Install libsvm
#===============================
RUN cd /app/download && \
        git clone https://github.com/cjlin1/libsvm.git && \
        cd libsvm && \
        make all && \
        /bin/cp -f svm-predict svm-scale svm-train /usr/local/bin/

#================================
# Install HMMER 
#===============================
RUN cd /app/download && \
        wget http://eddylab.org/software/hmmer3/3.1b2/hmmer-3.1b2-linux-intel-x86_64.tar.gz -O  hmmer-3.1b2-linux-intel-x86_64.tar.gz && \
        tar -xvzf  hmmer-3.1b2-linux-intel-x86_64.tar.gz  && \
        cd hmmer-3.1b2-linux-intel-x86_64 && \
        ./configure && \
        make && \
        make install

#================================
# Install PRODRES  
#===============================
RUN cd /app &&\
        git clone https://github.com/ElofssonLab/PRODRES &&\
        cd PRODRES &&\
        ln -s /data/db_prodres databases &&\
    cd /app/subcons/apps &&\
    ln -s ../../PRODRES PRODRES

#================================
# Install loctree2  
#===============================
RUN apt-get install -y pp-popularity-contest \
                       software-properties-common \
                       python-software-properties
RUN apt-add-repository "deb http://rostlab.org/debian/ stable main contrib non-free"
RUN apt-get update -y
RUN apt-get install -y  --allow-unauthenticated rostlab-debian-keyring
RUN apt-get update -y
RUN apt-get install -y loctree2
# link data
RUN cd /usr/share &&\
        ln -s /data/db_subcons/loctree2-data-1.0.2 loctree2-data

#================================
# Install R packages 
#===============================
RUN R -e "install.packages(c('ggplot2', 'reshape'), repos='http://ftp.acc.umu.se/mirror/CRAN/')"

#================================
# Install python package 
#===============================
RUN pip install --upgrade pip
RUN pip install biopython==1.70 \
                matplotlib==1.5.3 \
                html5lib==1.0.1 \
                mechanize==0.4.5 \
                pandas==0.19.1 \
                numpy==1.11.2 \
                scipy==0.18.1 \
                scikit-learn==0.17.1 \
                bokeh==0.12.5

#================================
# Install perl packages
#===============================
RUN apt-get install -y perlbrew &&\
    perlbrew install-cpanm &&\
    /root/perl5/perlbrew/bin/cpanm Bio::Perl Moose IPC::Run

#================================
# Install MultiLoc2 and SherLoc2
#===============================
RUN cd /app/subcons && \
        python TOOLS/MultiLoc2/configureML2.py &&\
    cd /app/subcons && \
        python TOOLS/SherLoc2/configureSL2.py

#================================
# Clean
#===============================
RUN rm -rf /app/download

ENV USER_DIRS "/app"

CMD ["/bin/bash" ]
